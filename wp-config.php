<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'demo_wp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '@2J_~J~4|R>GI( hAoDu~@F0s#/o^YBw/zeGlD%>(Z.## 8B%f~o;7Qop?`IV_+v' );
define( 'SECURE_AUTH_KEY',  '`]{<4GowBCT2d$f_Q{*Vc RR[[)60y4L!c]|%|n;*D2:E0)cC0pV@,Rnvtb/89qx' );
define( 'LOGGED_IN_KEY',    'R-4Zt#*x>j1&Medc(Eb%]5z(Q~,U&Asln=Zq:8<7u.kBC$D&V`q_vuw7o1 5<!d_' );
define( 'NONCE_KEY',        '-O/J5y4vsUQsN9[Hux6IRdhtRAH w#,3{6bexUOzRYl~]o8/eT (.8$J;,/j~D6>' );
define( 'AUTH_SALT',        'H1cCn/ sq,9ut[?jXsE+X )9k] c<]cD,)2|q$ (z#l|e76s1az8G^`hqmgwH=mE' );
define( 'SECURE_AUTH_SALT', 'jgpyuQM=wl,q(5!~n88M0h-v9mZ0THk:$1Bj?WF>x8sz~Ggx|#=9>)z7<rp!QJ Y' );
define( 'LOGGED_IN_SALT',   'm?@+dsG%1Bk*C|:>O0]y$#twfjr;;?~kwMdQ]&A8hF]q]j6#?UDHE# E$K9{.t,@' );
define( 'NONCE_SALT',       '~1Y-V^itGeb?f?}4i6h]~S$0[X=3}keT:En8N^.^qY7&{O)@T14(vot4qCEv=b`[' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
